#include <cstddef>
#include <cstdlib>
#include <string>
#include <iostream>
#include <chrono>

#include "Vector.h"
#include "LinkedList.h"

int main(int argc, char** argv)
{
  using namespace std::chrono;

  int testCount = argc > 1 ? atoi(argv[1]) : 10000;

  high_resolution_clock::time_point start, end;
  
  std::cout << "PREPEND" << std::endl;

  aisdi::Vector<int> vec;
  start = high_resolution_clock::now();
  for (int i = 0; i < testCount; i++)
  {
    vec.prepend(i);
  }
  end = high_resolution_clock::now();
  std::cout << "Vector - " << duration_cast<duration<double>>(end - start).count() << " s" << std::endl;

  aisdi::LinkedList<int> list;
  start = high_resolution_clock::now();
  for (int i = 0; i < testCount; i++)
  {
    list.prepend(i);
  }
  end = high_resolution_clock::now();
  std::cout << "LinkedList - " << duration_cast<duration<double>>(end - start).count() << " s" <<  std::endl;

  std::cout << std::endl;
  
  std::cout << "POP LAST" << std::endl;

  start = high_resolution_clock::now();
  while (!vec.isEmpty()) vec.popLast();
  end = high_resolution_clock::now();
  std::cout << "Vector - " <<  duration_cast<duration<double>>(end - start).count() << " s" <<  std::endl;

  start = high_resolution_clock::now();
  while(!list.isEmpty()) list.popLast();
  end = high_resolution_clock::now();
  std::cout << "LinkedList - " <<  duration_cast<duration<double>>(end - start).count() << " s" <<  std::endl;

  std::cout << std::endl;

  std::cout << "INSERT MIDDLE" << std::endl;
  
  start = high_resolution_clock::now();
  for (int i = 0; i < testCount; i++)
  {
    aisdi::Vector<int>::iterator iter = vec.begin() + vec.getSize() / 2;
    vec.insert(iter, i);
  }
  end = high_resolution_clock::now();
  std::cout << "Vector - " <<  duration_cast<duration<double>>(end - start).count() << " s" <<  std::endl;
  
  start = high_resolution_clock::now();
  for (int i = 0; i < testCount; i++)
  {
    aisdi::LinkedList<int>::iterator iter = list.begin() + list.getSize() / 2;
    list.insert(iter, i); 
  }
  end = high_resolution_clock::now();
  std::cout << "LinkedList - " <<  duration_cast<duration<double>>(end - start).count() << " s" <<  std::endl;
  
  std::cout << std::endl;

  std::cout << "ERASE ALL" << std::endl;

  start = high_resolution_clock::now();
  vec.erase(vec.begin(), vec.end());
  end = high_resolution_clock::now();
  std::cout << "Vector - " <<  duration_cast<duration<double>>(end - start).count() << " s" <<  std::endl;

  start = high_resolution_clock::now();
  list.erase(list.begin(), list.end());
  end = high_resolution_clock::now();
  std::cout << "LinkedList - " <<  duration_cast<duration<double>>(end - start).count() << " s" <<  std::endl;

  std::cout << std::endl;

  return 0;
}
