#ifndef AISDI_LINEAR_VECTOR_H
#define AISDI_LINEAR_VECTOR_H

#include <cstddef>
#include <initializer_list>
#include <stdexcept>
#include <cstdlib>

#include <iostream>

namespace aisdi
{
template <typename Type>
class Vector
{
public:
  using difference_type = std::ptrdiff_t;
  using size_type = std::size_t;
  using value_type = Type;
  using pointer = Type *;
  using reference = Type &;
  using const_pointer = const Type *;
  using const_reference = const Type &;

  class ConstIterator;
  class Iterator;
  using iterator = Iterator;
  using const_iterator = ConstIterator;

  const size_type INITIAL_CAPACITY = 100;

private:
  size_type m_capacity; // total current capacity
  size_type m_size; // current size
  char* m_memory; // buffer for objects

  void initializeArray(int initialCapacity = 16)
  {
    m_capacity = initialCapacity;
    m_size = 0;
    m_memory = new char[m_capacity * sizeof(Type)];
  }
  void freeAll()
  {
    if (m_memory != nullptr)
    {
      for (size_t i = 0; i < m_size; i++)
      {
        getPointerTo(i)->~Type();
      }   
      delete[] m_memory;
      m_memory = nullptr;
    }
  }
  void copyFrom(const Vector& other)
  {
    initializeArray(other.m_capacity);
    for (iterator i = other.begin(); i != other.end(); i++)
    {
      append(*i);
    }
  }
  void moveFrom(Vector&& other)
  {
    m_capacity = other.m_capacity;
    m_size = other.m_size;
    m_memory = other.m_memory;

    other.m_capacity = 0;
    other.m_size = 0;
    other.m_memory = nullptr;
  }
  pointer getPointerTo(size_t index) const
  {
    return reinterpret_cast<Type*>(m_memory + index * sizeof(Type));
  }

  Type* getBeginPointer() const
  {
    return reinterpret_cast<Type*>(m_memory);
  }
  Type* getEndPointer() const
  {
    return reinterpret_cast<Type*>(m_memory + m_size * sizeof(Type));
  }

public:

  Vector()
  {
    initializeArray();
  }

  Vector(std::initializer_list<Type> l)
  {
    initializeArray();
    for (const Type& i : l)
    {
      append(i);
    }
  }

  Vector(const Vector &other)
  {
    copyFrom(other);
  }

  Vector(Vector &&other)
  {
    moveFrom(std::move(other));
  }

  ~Vector()
  {
    freeAll();
  }

  Vector &operator=(const Vector &other)
  {
    if (this != &other)
    {
      freeAll();
      copyFrom(other);
    }

    return *this;
  }

  Vector &operator=(Vector &&other)
  {
    freeAll();
    moveFrom(std::move(other));
    return *this;
  }

  bool isEmpty() const
  {
    return m_size == 0;
  }

  size_type getSize() const
  {
    return m_size;
  }

  void append(const Type &item)
  {
    if (m_size == m_capacity)
    {
      m_capacity *= 2;
      char* newMemory = new char[m_capacity * sizeof(Type)];

      std::copy(m_memory, m_memory + m_size * sizeof(Type), newMemory);

      delete[] m_memory;
      m_memory = newMemory;
    }

    new(m_memory + m_size * sizeof(Type))Type(item); // construct new item

    m_size++;
  }

  void prepend(const Type &item)
  {
    if (m_capacity == m_size)
    {
      m_capacity *= 2;
      char* newMemory = new char[m_capacity * sizeof(Type)];

      std::copy(m_memory, m_memory + m_size * sizeof(Type), newMemory + sizeof(Type)); // leave one place free for new item

      delete[] m_memory;
      m_memory = newMemory;
    }
    else if (m_size != 0)
    {
      // move all elements, but start from the end to not overwrite previous elements
      for(int i = m_size - 1; i >= 0; i--)
      {
        std::copy(m_memory + i * sizeof(Type), m_memory + (i + 1) * sizeof(Type), m_memory + (i + 1) * sizeof(Type));
      }
    }

    new(m_memory)Type(item); // construct new item

    m_size++;
  }

  void insert(const const_iterator &insertPosition, const Type &item)
  {
    pointer ptr = insertPosition.getBasePointer();
    int index = static_cast<int>(ptr - getBeginPointer());
    char* targetMemory;

    if (m_capacity == m_size)
    {
      m_capacity *= 2;
      char* newMemory = new char[m_capacity * sizeof(Type)];

      if (ptr == getEndPointer())
      {
        std::copy(m_memory, m_memory + m_size * sizeof(Type), newMemory);
      }
      else
      {
        std::copy(m_memory, m_memory + index * sizeof(Type), newMemory);
        std::copy(m_memory + index * sizeof(Type), m_memory + m_size * sizeof(Type), newMemory + (index + 1) * sizeof(Type));
      }

      delete[] m_memory;
      m_memory = newMemory;

      targetMemory = m_memory + index * sizeof(Type);
    }
    else
    {
      if (ptr == getEndPointer())
      {
        // in case if points to end guard
        ptr = getPointerTo(m_size);
      }
      targetMemory = reinterpret_cast<char*>(ptr);

      if (index != static_cast<int>(m_size)) // if we don't insert at the end, we have to move data
      {
        //start moving elements from the end until we meet insertPosition. Item at insertPosition also will be moved to next position 
        for (char* memoryPtr = m_memory + (m_size - 1) * sizeof(Type); memoryPtr != targetMemory - sizeof(Type); memoryPtr -= sizeof(Type))
        {
          std::copy(memoryPtr, memoryPtr + sizeof(Type), memoryPtr + sizeof(Type));
        }
      }
    }

    new(targetMemory)Type(item); // construct item in memory block where targetMemory points to

    m_size++;
  }

  Type popFirst()
  {
    if (m_size == 0) throw std::logic_error("Collection is empty");

    Type* item = getBeginPointer();
    Type tmp = *item;
    item->~Type();
    
    std::copy(m_memory + sizeof(Type), m_memory + m_size * sizeof(Type), m_memory); // move all items to left

    m_size--;

    return tmp;
  }

  Type popLast()
  {
    if (m_size == 0) throw std::logic_error("Collection is empty");

    Type* item = getEndPointer();
    item--;
    Type tmp = *item;
    item->~Type();

    m_size--;

    return tmp;
  }

  void erase(const const_iterator &possition)
  {
    pointer ptr = possition.getBasePointer();

    if (ptr == getEndPointer()) throw std::out_of_range("Iterator does not point to valid item");

    pointer destructPtr = ptr;
    while (destructPtr++ != getEndPointer())
    {
      destructPtr->~Type();
    }
    
    char* memoryPosition = reinterpret_cast<char*>(ptr);
    std::copy(memoryPosition + sizeof(Type), m_memory + m_size * sizeof(Type), memoryPosition);

    m_size--;
  }

  void erase(const const_iterator &firstIncluded, const const_iterator &lastExcluded)
  {
    pointer firstPtr = firstIncluded.getBasePointer();
    pointer lastPtr = lastExcluded.getBasePointer();

    pointer destructPtr = firstPtr;
    while (destructPtr++ != lastPtr)
    {
      destructPtr->~Type();
    }

    int count = static_cast<int>(lastPtr - firstPtr);

    if (lastPtr != getEndPointer())
    {
      // if it's not until end then we need to move items
      char* memoryFirst = reinterpret_cast<char*>(firstPtr);
      char* memoryLast = reinterpret_cast<char*>(lastPtr);

      std::copy(memoryLast, m_memory + m_size * sizeof(Type), memoryFirst);
    }
    m_size -= count;
  }

  iterator begin()
  {
    return iterator(getBeginPointer(), *this);
  }

  iterator end()
  {
    return iterator(getEndPointer(), *this);
  }

  const_iterator cbegin() const
  {
    return const_iterator(getBeginPointer(), *this);
  }

  const_iterator cend() const
  {
    return const_iterator(getEndPointer(), *this);
  }

  const_iterator begin() const
  {
    return cbegin();
  }

  const_iterator end() const
  {
    return cend();
  }
};

template <typename Type>
class Vector<Type>::ConstIterator
{
friend class Vector<Type>;

public:
  using iterator_category = std::bidirectional_iterator_tag;
  using value_type = typename Vector::value_type;
  using difference_type = typename Vector::difference_type;
  using pointer = typename Vector::const_pointer;
  using reference = typename Vector::const_reference;

private:
  Type* m_ptr;
  const Vector<Type>& m_owner;

  Type* getBasePointer() const
  {
    return m_ptr;
  }

public:
  explicit ConstIterator(Type* ptr, const Vector<Type>& owner) : m_ptr(ptr), m_owner(owner)
  {
  }

  reference operator*() const
  {
    if (m_ptr == m_owner.cend().getBasePointer())
    {
      throw std::out_of_range("This iterator does not point to valid item.");
    }

    return *m_ptr;
  }

  ConstIterator &operator++()
  {
    if (m_ptr == m_owner.cend().getBasePointer())
    {
      throw std::out_of_range("Tried to move iterator after guard item.");
    }

    this->m_ptr++;
    return *this;
  }

  ConstIterator operator++(int)
  {
    ConstIterator tmp(m_ptr, m_owner);

    ++(*this);

    return tmp;
  }

  ConstIterator &operator--()
  {
    if (m_ptr == m_owner.cbegin().getBasePointer())
    {
      throw std::out_of_range("Tried to move iterator before first item.");
    }

    m_ptr--; 

    return *this;
  }

  ConstIterator operator--(int)
  {
    ConstIterator tmp(m_ptr, m_owner);
    
    --(*this);
    
    return tmp;
  }

  ConstIterator operator+(difference_type d) const
  {
    ConstIterator result(m_ptr + d, m_owner);
    return result;
  }

  ConstIterator operator-(difference_type d) const
  {
    ConstIterator result(m_ptr - d, m_owner);
    return result;
  }

  bool operator==(const ConstIterator &other) const
  {
    return m_ptr == other.m_ptr;
  }

  bool operator!=(const ConstIterator &other) const
  {
    return !ConstIterator::operator==(other);
  }
};

template <typename Type>
class Vector<Type>::Iterator : public Vector<Type>::ConstIterator
{
public:
  using pointer = typename Vector::pointer;
  using reference = typename Vector::reference;
  
public:
  explicit Iterator(pointer ptr, const Vector<Type>& owner) : ConstIterator(ptr, owner)
  {
  }

  Iterator(const ConstIterator &other)
      : ConstIterator(other)
  {
  }

  Iterator &operator++()
  {
    ConstIterator::operator++();
    return *this;
  }

  Iterator operator++(int)
  {
    auto result = *this;
    ConstIterator::operator++();
    return result;
  }

  Iterator &operator--()
  {
    ConstIterator::operator--();
    return *this;
  }

  Iterator operator--(int)
  {
    auto result = *this;
    ConstIterator::operator--();
    return result;
  }

  Iterator operator+(difference_type d) const
  {
    return ConstIterator::operator+(d);
  }

  Iterator operator-(difference_type d) const
  {
    return ConstIterator::operator-(d);
  }

  reference operator*() const
  {
    // ugly cast, yet reduces code duplication.
    return const_cast<reference>(ConstIterator::operator*());
  }
};
}

#endif // AISDI_LINEAR_VECTOR_H
